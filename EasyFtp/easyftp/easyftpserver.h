#ifndef EASYFTPSERVER_H
#define EASYFTPSERVER_H

#include <stdio.h>
#include "setting_parser.h"

struct _EasyFtpServer
{
    struct _EasyFtpSetting setting;
    int listenFd;
};

int initEasyFtpServer(struct _EasyFtpServer *server, const char *configFile);
int startEasyFtpServer(struct _EasyFtpServer *server);
int releaseEasyFtpServer(struct _EasyFtpServer *server);

#endif // EASYFTPSERVER_H
