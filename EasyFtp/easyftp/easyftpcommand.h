#ifndef EASYFTPCOMMAND_H
#define EASYFTPCOMMAND_H

#include <arpa/inet.h>
#include "easyftpclient.h"
#include "config.h"

struct _EasyFtpCommand
{
    char *name;
    char *syntax;
    void (*function)(struct _EasyFtpClient*, const char *);
    enum _EasyFtpClientState needed_state;
    char showInFeatCommand;
};

void efc_getCmdSocketAddr(struct _EasyFtpClient *client);
void efc_sayHiToClient(struct _EasyFtpClient *client);
int efc_processCommand(struct _EasyFtpClient *client, const char *command);


//private !!!!!!---------------------------------------------------------
void command_user(struct _EasyFtpClient *client, const char *arg);
void command_pass(struct _EasyFtpClient *client, const char *arg);
void command_pwd(struct _EasyFtpClient *client, const char *arg);
void command_type(struct _EasyFtpClient *client, const char *arg);
void command_port(struct _EasyFtpClient *client, const char *arg);
void command_eprt(struct _EasyFtpClient *client, const char *arg);
void command_pasv(struct _EasyFtpClient *client, const char *arg);
void command_epsv(struct _EasyFtpClient *client, const char *arg);
void command_allo(struct _EasyFtpClient *client, const char *arg);
void command_mput(struct _EasyFtpClient *client, const char *arg);
void command_stor(struct _EasyFtpClient *client, const char *arg);
void command_appe(struct _EasyFtpClient *client, const char *arg);
void command_mget(struct _EasyFtpClient *client, const char *arg);
void command_retr(struct _EasyFtpClient *client, const char *arg);
void command_list(struct _EasyFtpClient *client, const char *arg);
void command_nlst(struct _EasyFtpClient *client, const char *arg);
void command_syst(struct _EasyFtpClient *client, const char *arg);
void command_mdtm(struct _EasyFtpClient *client, const char *arg);
void command_cwd(struct _EasyFtpClient *client, const char *arg);
void command_cdup(struct _EasyFtpClient *client, const char *arg);
void command_dele(struct _EasyFtpClient *client, const char *arg);
void command_mkd(struct _EasyFtpClient *client, const char *arg);
void command_rmd(struct _EasyFtpClient *client, const char *arg);
void command_noop(struct _EasyFtpClient *client, const char *arg);
void command_rnfr(struct _EasyFtpClient *client, const char *arg);
void command_rnto(struct _EasyFtpClient *client, const char *arg);
void command_rest(struct _EasyFtpClient *client, const char *arg);
void command_size(struct _EasyFtpClient *client, const char *arg);
void command_quit(struct _EasyFtpClient *client, const char *arg);
void command_help(struct _EasyFtpClient *client, const char *arg);
void command_stat(struct _EasyFtpClient *client, const char *arg);
void command_site(struct _EasyFtpClient *client, const char *arg);
void command_chmod(struct _EasyFtpClient *client, const char *arg);
void command_opts(struct _EasyFtpClient *client, const char *arg);
void command_feat(struct _EasyFtpClient *client, const char *arg);

#endif // EASYFTPCOMMAND_H
