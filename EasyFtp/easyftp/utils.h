#ifndef UTILS_H
#define UTILS_H

#include "easyftpclient.h"

/*
Search through str looking for what. Replace any what with
the contents of by. Do not let the string get longer than max_length.
*/
int replace_string(char *str, char *what, char *by, int max_length);

/*
 * map client path to server path
 */
char *mapClientPathToServer(struct _EasyFtpClient *client, const char *filename);

/*
 * respond server status to client
 */
void respondClient(struct _EasyFtpClient *client, char *format, ...);

/*
 * set everything need to set to a socket
 */
void prepare_socket(int sock);

/*
 * find the first non_space character from startPos to end
 */
const char *findArg(const char *command, int startPos);

/*
 * create the data socket for data transfering
 */
int create_data_transfer_socket(struct _EasyFtpClient *client);

/*
 * store data recv from client
 */
void do_stor(struct _EasyFtpClient *client, const char *filename, int flags);

void easy_ftp_stat(char *name, FILE * client);
void dir_list_one_file(char *name, FILE *client, char verbose);
/*
 * dir list
 */
void dir_list(struct _EasyFtpClient *ftpClient, char *name, FILE * client, char verbose, int show_hidden);

/*
 * adapter to call dir_list
 */
void do_dir_list(struct _EasyFtpClient *client, const char *dirname, char verbose);

/*
 * change current work dir
 */
int easy_ftp_change_current_working_dir(struct _EasyFtpClient *client, const char *dirname);

#endif // UTILS_H
