#include "easyftpclient.h"
#include <stdlib.h>
#include <pthread.h>
#include <stdio.h>
#include <malloc.h>
#include <unistd.h>
#include <string.h>
#include "easyftplogger.h"
#include "easyftpcommand.h"
#include "config.h"

int handleClientRequest(int socketFd, struct _EasyFtpSetting *setting)
{
    pthread_t handleId;
    struct _EasyFtpClient *client;

    client = (struct _EasyFtpClient *)malloc(sizeof(struct _EasyFtpClient));
    if(client == NULL)
    {
        efwarning("handleClientRequest: malloc failed.\n");
        return -1;
    }

    client->cmdSocketFd = socketFd;
    client->state = EFCS_CONNECTED;
    strcpy(client->ftpWorkDir, setting->defaultWorkingDir);
    strcpy(client->curWorkDir, "/");
    client->xferType = EFXT_ASCII;
    client->xferBufferSize = setting->xferBufferSize;
    client->xferOffset = 0;
    client->xferRecvedSize = 0;
    client->xferTimeout = setting->xferTimeout;
    client->epsvAll = 0;
    client->pasvListenSocketFd = -1;
    client->pasvReady = 0;
    client->dataSocketFd = -1;
    client->pasvPortStart = setting->pasvPortStart;
    client->pasvPortEnd = setting->pasvPortEnd;
    client->dataPort = setting->dataPort;
    client->showHiddenFile = setting->showHiddenFile;
    client->showNonReadableFile = setting->showNonReadableFile;
    client->umaskForNew = setting->umaskForNew;
    client->pathToBeRename = NULL;
    client->quitMessage = setting->quitMessage;

    return pthread_create(&handleId, NULL, client_request_handle_thread, client);
}

void *client_request_handle_thread(void *arg)
{
    int ret = 0;
    char command[MAX_COMMAND_LENGTH + 1];
    struct _EasyFtpClient *client = (struct _EasyFtpClient *)arg;
    FILE *dataStream = fdopen(client->cmdSocketFd, "w");

    efc_getCmdSocketAddr(client);
    efc_sayHiToClient(client);

    while(1)
    {
        ret = read_line(client->cmdSocketFd, command, MAX_COMMAND_LENGTH);
        if(ret < 0)
        {
            efinfo("client_request_handle_thread: failed to read client data.\n");
            break;
        }

        ret = efc_processCommand(client, command);
        if(ret < 0)
        {
           efinfo("client_request_handle_thread: failed to process command: %s.\n", command);
           break;
        }

        if(client->state == EFCS_QUIT)
        {
            efdebug("client_request_handle_thread: client quit.\n");
            break;
        }
    }

    fclose(dataStream);
    close(client->cmdSocketFd);
    free(client);

    return NULL;
}

int read_line(int fd, char *buffer, int max_buffer_len)
{
    register int i;
    register char *cs;

    cs = buffer;
    for(i = 0; i < max_buffer_len; ++i)
    {
        if(read(fd, cs + i, 1) != 1)
        {
            return -1;
        }

        if(cs[i] == '\n')
        {
            break;
        }
    }

    if((i > 0) && (cs[i - 1] == '\r'))
    {
        i--;
    }

    cs[i] = '\0';

    return i;
}
