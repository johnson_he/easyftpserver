#ifndef SETTING_PARSER_H
#define SETTING_PARSER_H

#include "config.h"

struct _EasyFtpSetting
{
    int listenPort;
    char defaultWorkingDir[MAX_DIR_LENGTH];
    int pasvPortStart;
    int pasvPortEnd;
    int dataPort;
    int xferBufferSize;
    int xferTimeout;
    int showHiddenFile;
    int showNonReadableFile;
    int umaskForNew;
    const char *quitMessage;
};

int parse_setting(struct _EasyFtpSetting* setting, const char *path);

#endif // SETTING_PARSER_H
