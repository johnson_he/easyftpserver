#ifndef EASYFTPCLIENT_H
#define EASYFTPCLIENT_H

#include <arpa/inet.h>
#include "easyftpserver.h"

enum _EasyFtpClientState
{
    EFCS_CONNECTED = 0,
    EFCS_USERGOT,
    EFCS_AUTHORIZED,
    EFCS_RENAME,
    EFCS_QUIT
};

enum _EasyFtpXFerType
{
    EFXT_ASCII = 0,
    EFXT_BINARY
};

struct _EasyFtpClient
{
    int cmdSocketFd;
    struct sockaddr_in cmdSocketAddr;

    enum _EasyFtpClientState state;
    char ftpWorkDir[MAX_DIR_LENGTH];
    char curWorkDir[MAX_DIR_LENGTH];

    enum _EasyFtpXFerType xferType;
    int xferBufferSize;
    int xferOffset;
    int xferRecvedSize;
    int xferTimeout;

    struct sockaddr_in dataSocketAddr;
    int dataSocketFd;

    int epsvAll;
    int pasvListenSocketFd;
    int pasvReady;
    int pasvPortStart;
    int pasvPortEnd;

    int dataPort;

    int showHiddenFile;
    int showNonReadableFile;
    int umaskForNew;

    char *pathToBeRename;

    const char *quitMessage;
};

int handleClientRequest(int socketFd, struct _EasyFtpSetting *setting);


//private !!! ---------------------------------
void *client_request_handle_thread(void *arg);
int read_line(int fd, char *buffer, int max_buffer_len);

#endif // EASYFTPCLIENT_H
