#include "easyftpcommand.h"
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <malloc.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <time.h>
#include <stdlib.h>
#include <fcntl.h>
#include "easyftplogger.h"
#include "utils.h"

static const struct _EasyFtpCommand efallCommands[] =
{
    {"USER", "<sp> username", command_user, EFCS_CONNECTED, 0},
    {"PASS", "<sp> password", command_pass, EFCS_USERGOT, 0},
    {"XPWD", "(returns cwd)", command_pwd, EFCS_AUTHORIZED, 1},
    {"PWD", "(returns cwd)", command_pwd, EFCS_AUTHORIZED, 0},
    {"TYPE", "<sp> type-code (A or I)", command_type, EFCS_AUTHORIZED, 0},
    {"PORT", "<sp> h1,h2,h3,h4,p1,p2", command_port, EFCS_AUTHORIZED, 0},
    {"EPRT", "<sp><d><net-prt><d><ip><d><tcp-prt><d>", command_eprt, EFCS_AUTHORIZED, 1},
    {"PASV", "(returns address/port)", command_pasv, EFCS_AUTHORIZED, 0},
    {"EPSV", "(returns address/post)", command_epsv, EFCS_AUTHORIZED, 1},
    {"ALLO", "<sp> size", command_allo, EFCS_AUTHORIZED, 1},
    {"STOR", "<sp> pathname", command_stor, EFCS_AUTHORIZED, 0},
    {"APPE", "<sp> pathname", command_appe, EFCS_AUTHORIZED, 1},
    {"RETR", "<sp> pathname", command_retr, EFCS_AUTHORIZED, 0},
    {"LIST", "[<sp> pathname]", command_list, EFCS_AUTHORIZED, 0},
    {"NLST", "[<sp> pathname]", command_nlst, EFCS_AUTHORIZED, 0},
    {"SYST", "(returns system type)", command_syst, EFCS_CONNECTED, 0},
    {"MDTM", "<sp> pathname", command_mdtm, EFCS_AUTHORIZED, 1},
    {"XCWD", "<sp> pathname", command_cwd, EFCS_AUTHORIZED, 1},
    {"CWD", "<sp> pathname", command_cwd, EFCS_AUTHORIZED, 0},
    {"XCUP", "(up one directory)", command_cdup, EFCS_AUTHORIZED, 1},
    {"CDUP", "(up one directory)", command_cdup, EFCS_AUTHORIZED, 0},
    {"DELE", "<sp> pathname", command_dele, EFCS_AUTHORIZED, 0},
    {"XMKD", "<sp> pathname", command_mkd, EFCS_AUTHORIZED, 1},
    {"MKD", "<sp> pathname", command_mkd, EFCS_AUTHORIZED, 0},
    {"XRMD", "<sp> pathname", command_rmd, EFCS_AUTHORIZED, 1},
    {"RMD", "<sp> pathname", command_rmd, EFCS_AUTHORIZED, 0},
    {"NOOP", "(no operation)", command_noop, EFCS_AUTHORIZED, 0},
    {"OPTS", "<sp> string <sp> val", command_opts, EFCS_AUTHORIZED, 0},
    {"RNFR", "<sp> pathname", command_rnfr, EFCS_AUTHORIZED, 0},
    {"RNTO", "<sp> pathname", command_rnto, EFCS_RENAME, 0},
    {"REST", "<sp> byte-count", command_rest, EFCS_AUTHORIZED, 1},
    {"SIZE", "<sp> pathname", command_size, EFCS_AUTHORIZED, 1},
    {"QUIT", "(close control connection)", command_quit, EFCS_CONNECTED, 0},
    {"HELP", "[<sp> command]", command_help, EFCS_AUTHORIZED, 0},
    {"STAT", "<sp> pathname", command_stat, EFCS_AUTHORIZED, 0},
    {"SITE", "<sp> string", command_site, EFCS_AUTHORIZED, 0},
    {"FEAT", "(returns list of extensions)", command_feat, EFCS_AUTHORIZED, 1},
/*    {"AUTH", "<sp> authtype", command_auth, STATE_CONNECTED, 0},
    {"ADMIN_LOGIN", "(admin)", command_adminlogin, STATE_CONNECTED, 0},*/
    {"MGET", "<sp> pathname", command_mget, EFCS_AUTHORIZED, 0},
    {"MPUT", "<sp> pathname", command_mput, EFCS_AUTHORIZED, 0},
    {NULL, NULL, NULL, 0, 0}
};

void efc_getCmdSocketAddr(struct _EasyFtpClient *client)
{
    socklen_t len = sizeof(client->cmdSocketAddr);
    getsockname(client->cmdSocketFd, (struct sockaddr *)&client->cmdSocketAddr, &len);
}

void efc_sayHiToClient(struct _EasyFtpClient *client)
{
    easy_ftp_change_current_working_dir(client, client->curWorkDir);
    respondClient(client, "220 welcome to use Easy Ftp!");
}

int efc_processCommand(struct _EasyFtpClient *client, const char *command)
{
    int i;
    const char *arg;
    const struct _EasyFtpCommand *cmd = NULL;

    for(i = 0; efallCommands[i].name; ++i)
    {
        cmd = efallCommands + i;
        if(! strncasecmp(command, cmd->name, strlen(cmd->name)))
        {
            //got the command
            arg = findArg(command, strlen(cmd->name));
            if(client->state >= cmd->needed_state)
            {
                cmd->function(client, arg);
                return 0;
            }
            else
            {
                switch(client->state)
                {
                case EFCS_CONNECTED:
                    respondClient(client, "503 USER expected.");
                    return 1;
                case EFCS_USERGOT:
                    respondClient(client, "503 PASS expected.");
                    return 1;
                case EFCS_AUTHORIZED:
                    respondClient(client, "503 RNFR before RNTO expected.");
                    return 1;
                default:
                    break;
                }
            }
        }
    }

    respondClient(client, "500 Unknown command: \"%s\"", command);

    return 0;
}

void command_user(struct _EasyFtpClient *client, const char *arg)
{
    if(client->state >= EFCS_USERGOT)
    {
        respondClient(client, "503 Username already given.");
        return;
    }
    // let every user in
    client->state = EFCS_USERGOT;
    respondClient(client, "331 Password please.");

    efinfo("command_user: %s is tring to login.\n", arg);
}

void command_pass(struct _EasyFtpClient *client, const char *arg)
{
    if(client->state >= EFCS_AUTHORIZED)
    {
        respondClient(client, "503 Already logged in.");
        return;
    }

    // don't check passwod
    client->state = EFCS_AUTHORIZED;
    respondClient(client, "230 User logged in.");

    efinfo("command_pass: using password: %s.\n", arg);
}

void command_pwd(struct _EasyFtpClient *client, const char *arg)
{
    respondClient(client, "257 \"%s\" is the current working directory.", client->curWorkDir);
    (void)arg;
}

void command_type(struct _EasyFtpClient *client, const char *arg)
{
    if ((*arg == 'A') || (*arg == 'a'))
    {
        respondClient(client, "200 Transfer type changed to ASCII");
        client->xferType = EFXT_ASCII;
    }
    else if ((*arg == 'I') || (*arg == 'i'))
    {
        respondClient(client, "200 Transfer type changed to BINARY");
        client->xferType = EFXT_BINARY;
    }
    else
    {
        respondClient(client, "500 Type '%c' not supported.", *arg);
    }
}

void command_port(struct _EasyFtpClient *client, const char *arg)
{
    unsigned long a0, a1, a2, a3, p0, p1, addr;
    if(client->epsvAll)
    {
        respondClient(client, "500 EPSV ALL has been called.");
        return;
    }

    sscanf(arg, "%lu,%lu,%lu,%lu,%lu,%lu", &a0, &a1, &a2, &a3, &p0, &p1);
    addr = htonl((a0 << 24) + (a1 << 16) + (a2 << 8) + a3);

    client->dataSocketAddr.sin_addr.s_addr =  addr;
    client->dataSocketAddr.sin_port = htons((p0 << 8) + p1);

    if(client->pasvReady)
    {
        close(client->dataSocketFd);
        client->pasvReady = 0;
    }

    respondClient(client, "200 PORT %lu.%lu.%lu.%lu:%lu OK",
               a0, a1, a2, a3, (p0 << 8) + p1);
}

void command_eprt(struct _EasyFtpClient *client, const char *arg)
{
    char delim;
    int af;
    char addr[51];
    char foo[20];
    int port;
    if (client->epsvAll) {
        respondClient(client, "500 EPSV ALL has been called.");
        return;
    }
    if (strlen(arg) < 5) {
        respondClient(client, "500 Syntax error.");
        return;
    }
    delim = arg[0];
    sprintf(foo, "%c%%i%c%%50[^%c]%c%%i%c", delim, delim, delim, delim, delim);
    if (sscanf(arg, foo, &af, addr, &port) < 3) {
        respondClient(client, "500 Syntax error.");
        return;
    }
    if (af != 1) {
        respondClient(client, "522 Protocol unsupported, use (1)");
        return;
    }
    client->dataSocketAddr.sin_addr.s_addr = inet_addr(addr);
    client->dataSocketAddr.sin_port = htons(port);

    if(client->pasvReady)
    {
        close(client->dataSocketFd);
        client->pasvReady = 0;
    }

    respondClient(client, "200 EPRT %s:%i OK", addr, port);
}

void command_pasv(struct _EasyFtpClient *client, const char *arg)
{
    int a1, a2, a3, a4;
    socklen_t namelen;
    struct sockaddr_in localsock;
    int i = 0, success = 0, port;

    if (client->epsvAll) {
        respondClient(client, "500 EPSV ALL has been called.");
        return;
    }

    client->pasvListenSocketFd = socket(AF_INET, SOCK_STREAM, 0);
    client->dataSocketAddr.sin_addr.s_addr = client->cmdSocketAddr.sin_addr.s_addr;
    client->dataSocketAddr.sin_family = AF_INET;

    if (client->pasvPortStart == 0) {
        /* bind to any port */
        client->dataSocketAddr.sin_port = 0;
        if (bind(client->pasvListenSocketFd, (struct sockaddr *) &client->dataSocketAddr, sizeof(client->dataSocketAddr)) == -1)
        {
            respondClient(client, "425-Error: Unable to bind data socket.\r\n425 %s", strerror(errno));
            return;
        }
    }
    else
    {
        for (i = 0;; ++i)
        {
            port = client->pasvPortStart + i;
            if (port > client->pasvPortEnd)
            {
                break;
            }
            client->dataSocketAddr.sin_port = htons(port);
            if (bind(client->pasvListenSocketFd, (struct sockaddr *) &client->dataSocketAddr, sizeof(client->dataSocketAddr)) == 0) {
                success = 1;
                efinfo("command_pasv: Successfully bound port %d\n", port);
                break;
            }
        }

        if (! success) {
            respondClient(client, "425 Error: Unable to bind data socket.");
            return;
        }

        prepare_socket(client->pasvListenSocketFd);
    }

    if (listen(client->pasvListenSocketFd, 1))
    {
        respondClient(client, "425-Error: Unable to make socket listen.\r\n425 %s",
                 strerror(errno));
        return;
    }

    namelen = sizeof(localsock);
    getsockname(client->pasvListenSocketFd, (struct sockaddr *) &localsock, (socklen_t *) &namelen);

    sscanf((char *) inet_ntoa(localsock.sin_addr), "%i.%i.%i.%i",
       &a1, &a2, &a3, &a4);

    respondClient(client, "227 Entering Passive Mode (%i,%i,%i,%i,%i,%i)", a1, a2, a3, a4,
             ntohs(localsock.sin_port) >> 8, ntohs(localsock.sin_port) & 0xFF);

    client->pasvReady = 1;

    (void)arg;
}

void command_epsv(struct _EasyFtpClient *client, const char *arg)
{
    struct sockaddr_in localsock;
    socklen_t namelen;
    int af;

    if (arg[0])
    {
        if (! strncasecmp(arg, "ALL", 3))
        {
            client->epsvAll = 1;
        }
        else
        {
            if (sscanf(arg, "%i", &af) < 1)
            {
                respondClient(client, "500 Syntax error.");
                return;
            }
            else
            {
                if (af != 1)
                {
                    respondClient(client, "522 Protocol unsupported, use (1)");
                    return;
                }
            }
        }
    }

    client->pasvListenSocketFd = socket(AF_INET, SOCK_STREAM, 0);
    client->dataSocketAddr.sin_addr.s_addr = INADDR_ANY;
    client->dataSocketAddr.sin_port = 0;
    client->dataSocketAddr.sin_family = AF_INET;

    if (bind(client->pasvListenSocketFd, (struct sockaddr *) &client->dataSocketAddr, sizeof(client->dataSocketAddr)) == -1)
    {
        respondClient(client, "500-Error: Unable to bind data socket.\r\n425 %s",
                 strerror(errno));
        return;
    }

    if (listen(client->pasvListenSocketFd, 1))
    {
        respondClient(client, "500-Error: Unable to make socket listen.\r\n425 %s",
                 strerror(errno));
        return;
    }

    namelen = sizeof(localsock);
    getsockname(client->pasvListenSocketFd, (struct sockaddr *) &localsock, (socklen_t *) &namelen);

    respondClient(client, "229 Entering extended passive mode (|||%i|)",
             ntohs(localsock.sin_port));

    client->pasvReady = 1;
}

void command_allo(struct _EasyFtpClient *client, const char *arg)
{
    command_noop(client, arg);
}

void command_mput(struct _EasyFtpClient *client, const char *arg)
{
    char filename[MAX_COMMAND_LENGTH];  /* single filename */
    int from_index, to_index;      /* position in "filenames" and "filename" */

    from_index = 0;     /* start at begining of filenames */
    memset(filename, 0, MAX_COMMAND_LENGTH);       /* clear filename */
    to_index = 0;

    /* go until we find a NULL character */
    while ( arg[from_index] > 0)
    {
       /* copy filename until we hit a space */
       if (arg[from_index] == ' ')
       {
          /* got a full filename */
          command_stor(client, filename);
          /* clear filename and reset to_index */
          to_index = 0;
          memset(filename, 0, MAX_COMMAND_LENGTH);

          while (arg[from_index] == ' ')
            from_index++;    /* goto next position */
       }

       /* if we haven't hit a space, then copy the letter */
       else
       {
          filename[to_index] = arg[from_index];
          to_index++;
          from_index++;
          /* if the next character is a NULL, then this is the end of the filename */
          if (! arg[from_index])
          {
             command_stor(client, filename);   /* get the file */
             to_index = 0;             /* reset filename index */
             memset(filename, 0, MAX_COMMAND_LENGTH);    /* clear filename buffer */
             from_index++;                /* goto next character */
          }
       }

       /* if the buffer is getting too big, then stop */
       if (to_index > (MAX_COMMAND_LENGTH - 2) )
       {
          efinfo("Error: Filename in '%s' too long.\n", arg);
          return;
       }
    }   /* end of while */
}

void command_stor(struct _EasyFtpClient *client, const char *arg)
{
    do_stor(client, arg, O_CREAT | O_WRONLY | O_TRUNC);
}

void command_appe(struct _EasyFtpClient *client, const char *arg)
{
    do_stor(client, arg, O_CREAT | O_WRONLY | O_TRUNC);
}

void command_mget(struct _EasyFtpClient *client, const char *arg)
{
    char filename[MAX_COMMAND_LENGTH];  /* single filename */
    int from_index, to_index;      /* position in "filenames" and "filename" */
    const char *filenames = arg;

    from_index = 0;     /* start at begining of filenames */
    memset(filename, 0, MAX_COMMAND_LENGTH);       /* clear filename */
    to_index = 0;

    /* go until we find a NULL character */
    while ( filenames[from_index] > 0)
    {
        /* copy filename until we hit a space */
        if (filenames[from_index] == ' ')
        {
          /* got a full filename */
          command_retr(client, filename);
          /* clear filename and reset to_index */
          to_index = 0;
          memset(filename, 0, MAX_COMMAND_LENGTH);

          while (filenames[from_index] == ' ')
            from_index++;    /* goto next position */
        }
        /* if we haven't hit a space, then copy the letter */
        else
        {
            filename[to_index] = filenames[from_index];
            to_index++;
            from_index++;
            /* if the next character is a NULL, then this is the end of the filename */
            if (! filenames[from_index])
            {
                command_retr(client, filename);   /* send the file */
                to_index = 0;             /* reset filename index */
                memset(filename, 0, MAX_COMMAND_LENGTH);    /* clear filename buffer */
                from_index++;                /* goto next character */
            }
        }

       /* if the buffer is getting too big, then stop */
       if (to_index > (MAX_COMMAND_LENGTH - 2) )
       {
            efwarning("command_mget: Filename in '%s' too long.\n", filenames);
            return;
       }
    }   /* end of while */
}

void command_retr(struct _EasyFtpClient *client, const char *arg)
{
    char *mapped = NULL;
    char *buffer;
    ssize_t send_status;
    const char *filename = arg;

    int phile;
    int i;
    struct stat statbuf;

    mapped = mapClientPathToServer(client, filename);
    if (! mapped)
    {
        efwarning("command_retr: Memory error in sending file.\n", 0);
        respondClient(client, "553 An unknown error occured on the server.", 9);
        return;
    }

    phile = open(mapped, O_RDONLY);
    if (phile == -1)
    {
        // failed to open a file
        efwarning("command_retr: '%s' while trying to receive file '%s'.\n",
                  strerror(errno), filename);
        respondClient(client, "553 Error: %s.", strerror(errno));
        free(mapped);
        return;
    }

    stat(mapped, (struct stat *) &statbuf);
    if (S_ISDIR(statbuf.st_mode))
    {
        respondClient(client, "550 Error: Is a directory.");
        free(mapped);
        return;
    }

    efinfo("command_retr: Client is receiving file '%s'.\n", filename);

    if (create_data_transfer_socket(client))
    {
        return;
    }

    lseek(phile, client->xferOffset, SEEK_SET);
    client->xferOffset = 0;
    buffer = malloc(client->xferBufferSize * 2 + 1);
    /* make sure buffer was created */
    if (! buffer)
    {
        respondClient(client, "553 An unknown error occured.");
        efwarning("command_retr: Memory error while trying to send file.", 0);
        close(client->dataSocketFd);
        close(phile);
        return;
    }

    while ((i = read(phile, buffer, client->xferBufferSize)))
    {
        if (client->xferType == EFXT_ASCII)
        {
            buffer[i] = '\0';
            i += replace_string(buffer, "\n", "\r\n", client->xferBufferSize);
        }
        send_status = send(client->dataSocketFd, buffer, i, 0);
        // check for dropped connection
        if (send_status < 0)
        {
            free(buffer);
            close(phile);
            close(client->dataSocketFd);
            respondClient(client, "426 Transfer aborted.");
            respondClient(client, "226 Aborted.");
            efwarning("command_retr: File transmission interrupted. Send failed.\n");
            return;
        }
    }       // end of while

    free(buffer);
    close(phile);
    close(client->dataSocketFd);
    client->xferOffset = 0;

    respondClient(client, "226 File transmission successful.");
    efwarning("command_retr: File transmission of '%s' successful.\n", filename);
    if(mapped)
    {
        free(mapped);
    }
    // Update_Send_Recv(user, bytes_sent, bytes_recvd);
}

void command_list(struct _EasyFtpClient *client, const char *arg)
{
    do_dir_list(client, arg, 1);
}

void command_nlst(struct _EasyFtpClient *client, const char *arg)
{
    do_dir_list(client, arg, 0);
}

void command_syst(struct _EasyFtpClient *client, const char *arg)
{
    respondClient(client, "215 UNIX Type: L8");
    (void)arg;
}

void command_mdtm(struct _EasyFtpClient *client, const char *arg)
{
    struct stat statbuf;
    struct tm *filetime;
    char *fullfilename = mapClientPathToServer(client, arg);
    if (!stat(fullfilename, (struct stat *) &statbuf))
    {
        filetime = gmtime((time_t *) & statbuf.st_mtime);
        respondClient(client, "213 %04i%02i%02i%02i%02i%02i",
                filetime->tm_year + 1900, filetime->tm_mon + 1,
                filetime->tm_mday, filetime->tm_hour, filetime->tm_min,
                filetime->tm_sec);
    } else {
        respondClient(client, "550 Error while determining the modification time: %s",
                strerror(errno));
    }
    free(fullfilename);
}

void command_cwd(struct _EasyFtpClient *client, const char *arg)
{
    if (easy_ftp_change_current_working_dir(client, arg))
    {
        efwarning("command_cwd: error:'%s', while changing directory to '%s'.\n",
                  strerror(errno), arg);
        respondClient(client, "451 Error: %s.", strerror(errno));
    }
    else
    {
        efinfo("command_cwd: Changed directory to '%s', result: %s.\n", arg, client->curWorkDir);
        respondClient(client, "250 OK");
    }
}

void command_cdup(struct _EasyFtpClient *client, const char *arg)
{
    efinfo("command_cdup: Changed directory to '..'.\n");
    easy_ftp_change_current_working_dir(client, "..");
    respondClient(client, "250 OK");
    (void)arg;
}

void command_dele(struct _EasyFtpClient *client, const char *arg)
{
    struct stat sbuf;
    char *mapped = mapClientPathToServer(client, arg);
    if (! mapped)
    {
        respondClient(client, "451 Error: Unable to perform delete..");
        return;
    }


    if ( lstat(mapped, &sbuf) == -1 )
    {
        respondClient(client, "451 Error: %s.", strerror(errno));
    }
    else
    {
        if (S_ISDIR(sbuf.st_mode))
        {
            efwarning("command_dele: '%s' while trying to delete folder '%s' with DELE.\n", strerror(errno), arg);
            respondClient(client, "550 %s: Is a directory", arg);
        }
        else
        {
            if (unlink(mapped))
            {
                efwarning("command_dele: '%s' while trying to delete file '%s'.\n", strerror(errno), arg);
                respondClient(client, "451 Error: %s.", strerror(errno));
            }
            else
            {
                efinfo("command_dele: Deleted file '%s'.\n", arg);
                respondClient(client, "200 OK");
            }
        }
    }

    free(mapped);
}

void command_mkd(struct _EasyFtpClient *client, const char *arg)
{
    char *mapped = mapClientPathToServer(client, arg);
    if (! mapped)
    {
       respondClient(client, "451 Error: Unable to create directory.");
       return;
    }

    if (mkdir(mapped, 0777))
    {
        efwarning("command_mkd: '%s' while trying to create directory '%s'.\n",
                  strerror(errno), arg);
        respondClient(client, "451 Error: %s.", strerror(errno));
    }
    else
    {
        efinfo("command_mkd: Created directory '%s'.\n", arg);
        respondClient(client, "257 \"%s\" has been created.", arg);
    }

    free(mapped);
}

void command_rmd(struct _EasyFtpClient *client, const char *arg)
{
    char *mapped = mapClientPathToServer(client, arg);
    if (! mapped)
    {
        respondClient(client, "451 Error: Unable to remove directory.");
        return;
    }

    if (rmdir(mapped))
    {
        if (errno == ENOTEMPTY)
        {
            efwarning("command_rmd: '%s' while trying to remove directory '%s': not empty.\n", strerror(errno), arg);
            respondClient(client, "550 %s: Directory not empty", arg);
        }
        else
        {
            efwarning("command_rmd: '%s' while trying to remove directory '%s'.\n", strerror(errno), arg);
            respondClient(client, "451 Error: %s.", strerror(errno));
        }

    }
    else
    {
        efinfo("command_rmd: Removed directory '%s'.\n", arg);
        respondClient(client, "250 OK");
    }

    free(mapped);
}

void command_noop(struct _EasyFtpClient *client, const char *arg)
{
    respondClient(client, "200 OK");
    (void)arg;
}

void command_rnfr(struct _EasyFtpClient *client, const char *arg)
{
    FILE *file;
    char *mapped = mapClientPathToServer(client, arg);
    if (! mapped)
    {
        respondClient(client, "451 Error: Unable to locate file.");
        return;
    }

    if ((file = fopen(mapped, "r")))
    {
        fclose(file);

        if (client->pathToBeRename)
        {
            free(client->pathToBeRename);
        }
        client->pathToBeRename = mapped;
        client->state = EFCS_RENAME;
        respondClient(client, "350 File exists, ready for destination name.");
    }
    else
    {
        free(mapped);
        respondClient(client, "451 Error: %s.", strerror(errno));
    }
}

void command_rnto(struct _EasyFtpClient *client, const char *arg)
{
    char *mapped = mapClientPathToServer(client, arg);
    if((! mapped) || (! client->pathToBeRename))
    {
       respondClient(client, "451 Error: Unable to rename file.");
       return;
    }

    if (rename(client->pathToBeRename, mapped))
    {
        efwarning("command_rnto: '%s' while trying to rename '%s' to '%s'.\n",
                strerror(errno), client->pathToBeRename, mapped);
        respondClient(client, "451 Error: %s.", strerror(errno));
    }
    else
    {
        efinfo("command_rnto: Successfully renamed '%s' to '%s'.\n",
                client->pathToBeRename, mapped);
        respondClient(client, "250 OK");
        client->state = EFCS_AUTHORIZED;
    }

    free(client->pathToBeRename);
    client->pathToBeRename = NULL;
    free(mapped);
}

void command_rest(struct _EasyFtpClient *client, const char *arg)
{
    client->xferOffset = strtoul(arg, NULL, 10);
    respondClient(client, "350 Restarting at offset %i.", client->xferOffset);
}

void command_size(struct _EasyFtpClient *client, const char *arg)
{
    struct stat statbuf;
    char *mapped = mapClientPathToServer(client, arg);
    if (! mapped)
    {
        respondClient(client, "451 Error: Unable to locate file.");
        return;
    }

    if (! stat(mapped, &statbuf))
    {
        respondClient(client, "213 %i", (int) statbuf.st_size);
    }
    else
    {
        respondClient(client, "550 Error: %s.", strerror(errno));
    }

    free(mapped);
}

void command_quit(struct _EasyFtpClient *client, const char *arg)
{
    respondClient(client, "221 %s", client->quitMessage);
    client->state = EFCS_QUIT;
    (void)arg;
}

void command_help(struct _EasyFtpClient *client, const char *arg)
{
    int i;
    if (arg[0] == '\0')
    {
        respondClient(client, "214-The following commands are recognized.");
        for (i = 0; efallCommands[i].name; i++)
        {
            respondClient(client, "214-%s", efallCommands[i].name);
        }
        respondClient(client, "214 End of help");
    }
    else
    {
        for (i = 0; efallCommands[i].name; i++)
        {
            if (! strcasecmp(arg, efallCommands[i].name))
            {
                respondClient(client, "214 Syntax: %s", efallCommands[i].syntax);
            }
        }
    }
}

void command_stat(struct _EasyFtpClient *client, const char *arg)
{
    FILE *dataStream;
    char *mapped = mapClientPathToServer(client, arg);
    if (! mapped)
    {
        respondClient(client, "451 Error: Unable to discover status.");
        return;
    }

    dataStream = fdopen(client->dataSocketFd, "w");

    respondClient(client, "213-Status of %s:", arg);
    easy_ftp_stat(mapped, dataStream);
    respondClient(client, "213 End of Status.");

    free(mapped);
    fclose(dataStream);
}

void command_site(struct _EasyFtpClient *client, const char *arg)
{
    respondClient(client, "550 SITE commands are disabled.");
    (void)arg;
}

void command_chmod(struct _EasyFtpClient *client, const char *arg)
{
    int permissions;
    char *mapped;
    char *filename;

    if (! strchr(arg, ' '))
    {
        respondClient(client, "550 Usage: SITE CHMOD <permissions> <filename>");
        return;
    }

    filename = strdup(strchr(arg, ' ') + 1);
    if (! filename)
    {
        respondClient(client, "550: An error occured on the server trying to CHMOD.");
        return;
    }

    mapped = mapClientPathToServer(client, filename);
    free(filename);

    *strchr(arg, ' ') = '\0';
    sscanf(arg, "%o", &permissions);
    if (chmod(mapped, permissions))
    {
        respondClient(client, "Error: %s.", strerror(errno));
    }
    else
    {
        efinfo("Changed permissions of '%s' to '%o'.\n", mapped, permissions);
        respondClient(client, "200 CHMOD successful.");
    }

    free(mapped);
}

void command_opts(struct _EasyFtpClient *client, const char *arg)
{
    if(! strcasecmp(arg, "utf8 on"))
    {
        respondClient(client, "200 UTF8 ON");
    }
    else
    {
        respondClient(client, "550 %s not implemented", arg);
    }
}

void command_feat(struct _EasyFtpClient *client, const char *arg)
{
    int i;
    respondClient(client, "211-Extensions supported:");
    for (i = 0; efallCommands[i].name; i++)
    {
        if (efallCommands[i].showInFeatCommand)
        {
            respondClient(client, " %s", efallCommands[i].name);
        }
    }
    respondClient(client, " UTF8");
    respondClient(client, "211 End");
    (void)arg;
}
