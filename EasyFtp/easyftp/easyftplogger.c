#include "easyftplogger.h"
#include <stdarg.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include "config.h"

static enum DBG_LEVEL __ef_log_level = DBG_LEVEL_DEBUG;
static int __ef_log_listen_socket = -1;
static pthread_t __ef_log_listen_thread_id;
pthread_mutex_t __ef_log_mutex;
static int __ef_log_fd = -1;

static void *ef_logger_listen_thread_fun(void *arg)
{
    int newSocket;
    struct sockaddr_in remoteAdd;
    socklen_t len = sizeof(remoteAdd);

    while(1)
    {
        newSocket = accept(__ef_log_listen_socket, (struct sockaddr *)&remoteAdd, &len);
        if(newSocket > 0)
        {
            pthread_mutex_lock(&__ef_log_mutex);
            if(__ef_log_fd >= 0)
            {
                close(__ef_log_fd);
            }
            __ef_log_fd = newSocket;
            pthread_mutex_unlock(&__ef_log_mutex);
        }
    }

    return arg;
}

static void handler_sigpipe(int signum)
{
    __ef_log_fd = -1;
}

int ef_init_logger()
{
#if DEBUG_USING_NETWORK_LOGGER
    int socketFd = -1;
    int i;
    struct sockaddr_in logAddr;

    socketFd = socket(AF_INET, SOCK_STREAM, 0);
    if(socketFd < 0)
    {
        printf("ef_init_logger: failed to create socket.\n");
        return -1;
    }

    i = 1;
    setsockopt(socketFd, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i));
    setsockopt(socketFd, SOL_SOCKET, SO_REUSEPORT, &i, sizeof(i));

    memset(&logAddr, 0, sizeof(logAddr));
    logAddr.sin_family = AF_INET;
    logAddr.sin_addr.s_addr = INADDR_ANY;
    logAddr.sin_port = htons(DEFAULT_LOGGER_LISTEN_PORT);

    if(bind(socketFd, (struct sockaddr *)&logAddr, sizeof(logAddr)) < 0)
    {
        printf("ef_init_logger: bind failed: %s.\n", strerror(errno));
        close(socketFd);
        return -2;
    }

    if(listen(socketFd, 1) < 0)
    {
        printf("ef_init_logger: listen failed: %s.\n", strerror(errno));
        close(socketFd);
        return -3;
    }

    if(pthread_mutex_init(&__ef_log_mutex, NULL))
    {
        printf("ef_init_logger: mutex init failed: %s.\n", strerror(errno));
        close(socketFd);
        return -4;
    }

    __ef_log_listen_socket = socketFd;

    signal(SIGPIPE, handler_sigpipe);

    return pthread_create(&__ef_log_listen_thread_id, NULL, ef_logger_listen_thread_fun, NULL);
#else
    __ef_log_fd = 1;

    return 0;
#endif
}

void ef_printf(enum DBG_LEVEL level, const char *fmt, ...)
{
    va_list args;
    char debugMessageBuffer[1024];

    if(level > __ef_log_level)
    {
        return;
    }

#if DEBUG_USING_NETWORK_LOGGER
    pthread_mutex_lock(&__ef_log_mutex);
#endif
    if(__ef_log_fd < 0)
    {
        pthread_mutex_unlock(&__ef_log_mutex);
        return;
    }

    va_start(args,fmt);
    vsprintf(debugMessageBuffer, fmt, args);
    va_end(args);

    write(__ef_log_fd, debugMessageBuffer, strlen(debugMessageBuffer));

#if DEBUG_USING_NETWORK_LOGGER
    write(__ef_log_fd, "\r\n", 2);
    pthread_mutex_unlock(&__ef_log_mutex);
#endif

    return;
}
