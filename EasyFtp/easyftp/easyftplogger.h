#ifndef EASYFTPLOGGER_H
#define EASYFTPLOGGER_H

enum DBG_LEVEL
{
    DBG_LEVEL_NONE = 1,
    DBG_LEVEL_FATAL,
    DBG_LEVEL_ERROR,
    DBG_LEVEL_WARNING,
    DBG_LEVEL_INFO,
    DBG_LEVEL_DEBUG,
    DBG_LEVEL_ALL
};

int ef_init_logger();
void ef_printf(enum DBG_LEVEL level, const char *fmt, ...);

#define effatal(format, args...)   ef_printf(DBG_LEVEL_FATAL, format, ##args)
#define eferror(format, args...)   ef_printf(DBG_LEVEL_ERROR, format, ##args)
#define efwarning(format, args...)   ef_printf(DBG_LEVEL_WARNING, format, ##args)
#define efinfo(format, args...)   ef_printf(DBG_LEVEL_INFO, format, ##args)
#define efdebug(format, args...)   ef_printf(DBG_LEVEL_DEBUG, format, ##args)

#endif // EASYFTPLOGGER_H
