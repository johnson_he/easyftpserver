#include "setting_parser.h"
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>

int parse_setting(struct _EasyFtpSetting* setting, const char *path)
{
    int ret = 0;
    DIR *dir = NULL;

    dir = opendir(path);
    if(dir == NULL)
    {
        efwarning("parse_setting: dir(%s) not exist, will try to use default path: %s.\n", path, DEFAULT_WORKING_DIR);
        strcpy(setting->defaultWorkingDir, DEFAULT_WORKING_DIR);
    }
    else
    {
        closedir(dir);
        strcpy(setting->defaultWorkingDir, path);
    }

    setting->listenPort = DEFAULT_LISTEN_PORT;
    setting->pasvPortStart = DEFAULT_PASV_PORT_START;   // set to 0, will listen to any port
    setting->pasvPortEnd = DEFAULT_PASV_PORT_END;
    setting->dataPort = DEFAULT_DATA_PORT;
    setting->xferBufferSize = DEFAULT_XFER_BUFFER_SIZE;
    setting->xferTimeout = DEFAULT_XFER_TIME_OUT;
    setting->showHiddenFile = DEFAULT_SHOW_HIDDEN_FILE;
    setting->showNonReadableFile = DEFAULT_SHOW_NON_READABLE_FILE;
    setting->umaskForNew = DEFAULT_UMASK_FOR_NEW_FILE;
    setting->quitMessage = DEFAULT_QUIT_MESSAGE;

    return ret;
}
