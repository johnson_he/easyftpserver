#include "test.h"
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include "utils.h"
#include "config.h"

#if DEBUG_IN_LINUX_PLATFORM
#define FAKE_CMD_FILE     "/home/johnson/workspace/easyFtp/cmd.log"
#else
#define FAKE_CMD_FILE     "/dev/null"
#endif

int testCommands()
{
    return testList();
}

int initTestClient(struct _EasyFtpClient *client)
{
    int ret = 0;

    ret = open(FAKE_CMD_FILE, O_WRONLY|O_CREAT|O_TRUNC, 0666);
    if(ret < 0)
    {
        eferror("initTestClient: failed to open cmd log file.\n");
        return -1;
    }

    client->cmdSocketFd = ret;
    strcpy(client->curWorkDir, "/");
    strcpy(client->ftpWorkDir, DEFAULT_WORKING_DIR);

    return 0;
}

void releaseTestClient(struct _EasyFtpClient *client)
{
    close(client->cmdSocketFd);
}

int testCWD()
{
    int ret = 0;
    struct _EasyFtpClient client;

    if(initTestClient(&client) < 0)
    {
        return -1;
    }

    //command_cwd(&client, "/");

    command_cwd(&client, "../easyFtp/../easyFtp/build-EasyFtp-Desktop_Qt_5_3_GCC_64bit-Debug");

    releaseTestClient(&client);
    return ret;
}

int testList()
{
    int ret = 0;
    struct _EasyFtpClient client;

    if(initTestClient(&client) < 0)
    {
        return -1;
    }

    easy_ftp_change_current_working_dir(&client, "/");

    command_list(&client, "");

    releaseTestClient(&client);
    return ret;
}
