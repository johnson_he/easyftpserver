#include "easyftpserver.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include "setting_parser.h"
#include "easyftplogger.h"
#include "easyftpclient.h"

int initEasyFtpServer(struct _EasyFtpServer *server, const char *configFile)
{
    int ret = 0;
    int socketfd = -1;
    struct sockaddr_in addr;
    int i;

    ret = parse_setting(&server->setting, configFile);
    if(ret < 0)
    {
        eferror("initEasyFtpServer: failed parse setting.\n");
        return -1;
    }

    server->listenFd = -1;
    socketfd = socket(AF_INET, SOCK_STREAM, 0);
    if(socketfd < 0)
    {
        eferror("initEasyFtpServer: failed to open socket.\n");
        return -2;
    }

    i = 1;
#ifdef SO_REUSEADDR
    setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i));
#endif
#ifdef SO_REUSEPORT
    setsockopt(socketfd, SOL_SOCKET, SO_REUSEPORT, &i, sizeof(i));
#endif

    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(server->setting.listenPort);
    addr.sin_addr.s_addr = INADDR_ANY;

    ret = bind(socketfd, (struct sockaddr *)&addr, sizeof(addr));
    if(ret < 0)
    {
        eferror("initEasyFtpServer: failed to bind to port: %d.\n", server->setting.listenPort);
        close(socketfd);
        return -3;
    }

    ret = listen(socketfd, 5);
    if(ret < 0)
    {
        eferror("initEasyFtpServer: failed to listen.\n");
        close(socketfd);
        return -4;
    }

    server->listenFd = socketfd;

    return 0;
}

int startEasyFtpServer(struct _EasyFtpServer *server)
{
    int ret = 0;
    int clientFd;
    struct sockaddr_in clientAddr;
    socklen_t addrLen = sizeof(clientAddr);

    while(1)
    {
        clientFd = accept(server->listenFd, (struct sockaddr *)&clientAddr, &addrLen);
        if(clientFd >= 0)
        {
            ret = handleClientRequest(clientFd, &server->setting);
            if(ret < 0)
            {
                efwarning("startEasyFtpServer: failed to handle client request from : %s.\n", inet_ntoa(clientAddr.sin_addr));
                continue;
            }
        }
        else
        {
            efinfo("startEasyFtpServer: accept failed.\n");
            break;
        }
    }

    return ret;
}

int releaseEasyFtpServer(struct _EasyFtpServer *server)
{
    if(server->listenFd >= 0)
    {
        close(server->listenFd);
    }

    return 0;
}
