#ifndef TEST_H
#define TEST_H

#include "easyftp/easyftpcommand.h"

int testCommands();


int initTestClient(struct _EasyFtpClient *client);
void releaseTestClient(struct _EasyFtpClient *client);
int testCWD();
int testList();

#endif // TEST_H
