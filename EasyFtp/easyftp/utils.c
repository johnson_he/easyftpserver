#include "utils.h"
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <fnmatch.h>
#include <stdarg.h>
#include <malloc.h>
#include <fcntl.h>
#include <stdlib.h>
#include "easyftplogger.h"
#include "config.h"

int replace_string(char *str, char *what, char *by, int max_length)
{
    char *foo, *bar = str;
    int i = 0;
    int str_length, what_length, by_length;

    /* do a sanity check */
    if (! str) return 0;
    if (! what) return 0;
    if (! by) return 0;

    what_length = strlen(what);
    by_length = strlen(by);
    str_length = strlen(str);

    foo = strstr(bar, what);
    /* keep replacing if there is somethign to replace and it
       will no over-flow
    */
    while ( (foo) &&
            ( (str_length + by_length - what_length) < (max_length - 1) ) )
    {
        bar = foo + strlen(by);
        memmove(bar, foo + strlen(what), strlen(foo + strlen(what)) + 1);
        memcpy(foo, by, strlen(by));
        i++;
        foo = strstr(bar, what);
        str_length = strlen(str);
    }
    return i;
}

int easy_ftp_append_single_dir(char *from, const char *to, int toLen)
{
    char *p = NULL;

    if(toLen < 0)
    {
        efwarning("easy_ftp_append_dir: toLen: %d.\n", toLen);
        return -1;
    }

    // handle the special case
    switch(toLen)
    {
    case 0:
        return 0;
        break;
    case 1:
        if((to[0] == '/') || (to[0] == '.')) // to = "/" or to = "."
        {
            //cd to current directory
            return 0;
        }
        break;
    case 2:
        if((to[0] == '.') && (to[1] == '.'))    // to = ".."
        {
            // from = "/", no up directory
            if(strcmp(from, "/") == 0)
            {
                return 0;
            }

            // from like "xxx/", skip the last '/' of from
            if(from[strlen(from) - 1] == '/')
            {
                from[strlen(from) - 1] = '\0';
            }

            // truncate the last directory
            p = strrchr(from, '/');
            if(p == from) // from like "/xxx"
            {
                from[1] = '\0';
                return 0;
            }
            else    // from like "/xxx/xxx"
            {
                *p = '\0';
                return 0;
            }
        }
        break;
    default:
        break;
    }

    // start appending directory
    if(to[0] == '/') // to like "/xxx"
    {
        strncat(from, to, toLen);
    }
    else    // to like "xxx"
    {
        if(from[strlen(from) - 1] != '/')
        {
            strcat(from, "/");
        }
        strncat(from, to, toLen);
    }

    return 0;
}

int easy_ftp_append_multi_dir(char *from, const char *to)
{
    int ret = 0;
    const char *plast, *pnext;

    plast = pnext = to;
    while(pnext)
    {
        pnext = strchr(plast, '/');
        if(pnext == NULL)
        {
            ret = easy_ftp_append_single_dir(from, plast, strlen(plast));
        }
        else
        {
            ret = easy_ftp_append_single_dir(from, plast, pnext - plast);
        }

        if(ret < 0)
        {
            return -1;
        }

        plast = pnext + 1;
    }

    return 0;
}

char *mapClientPathToServer(struct _EasyFtpClient *client, const char *filename)
{
    char *result = malloc(MAX_DIR_LENGTH);

    if (! result)
    {
       return NULL;
    }

    strcpy(result, client->ftpWorkDir);
    if(filename[0] != '/')
    {
        strcat(result, client->curWorkDir);
    }
    if(easy_ftp_append_multi_dir(result + strlen(client->ftpWorkDir), filename) < 0)
    {
        free(result);
        return NULL;
    }

    return result;
}

void respondClient(struct _EasyFtpClient *client, char *format, ...)
{
    char buffer[MAX_REPSOND_BUFFER_LENGTH];
    va_list val;
    va_start(val, format);
    vsnprintf(buffer, MAX_REPSOND_BUFFER_LENGTH, format, val);
    va_end(val);

    strcat(buffer, "\r\n");
    write(client->cmdSocketFd, buffer, strlen(buffer));
}

void prepare_socket(int sock)
{
    int on = 1;
#ifdef TCP_NODELAY
    setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (void *) &on, sizeof(on));
#endif
#ifdef TCP_NOPUSH
    setsockopt(sock, IPPROTO_TCP, TCP_NOPUSH, (void *) &on, sizeof(on));
#endif
#ifdef SO_REUSEADDR
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (void *) &on, sizeof(on));
#endif
#ifdef SO_REUSEPORT
    setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, (void *) &on, sizeof(on));
#endif
#ifdef SO_SNDBUF
    on = 65536;
    setsockopt(sock, SOL_SOCKET, SO_SNDBUF, (void *) &on, sizeof(on));
#endif
}

const char *findArg(const char *command, int startPos)
{
    const char *arg = command + startPos;

    while((*arg) && ((*arg == ' ') || (*arg == '\t')))
    {
        arg++;
    }

    return arg;
}

int create_data_transfer_socket(struct _EasyFtpClient *client)
{
    struct sockaddr foo;
    struct sockaddr_in local;
    socklen_t namelen = sizeof(foo);
    int curuid = geteuid();

    memset(&foo, 0, sizeof(foo));
    memset(&local, 0, sizeof(local));

    if (client->pasvReady)
    {
        client->dataSocketFd = accept(client->pasvListenSocketFd, (struct sockaddr *) &foo, (socklen_t *) &namelen);
        if (client->dataSocketFd == -1)
        {
            respondClient(client, "425-Unable to accept data connection.\r\n425 %s.",
                     strerror(errno));
            return 1;
        }

        close(client->pasvListenSocketFd);
        prepare_socket(client->dataSocketFd);
    }
    else
    {
        client->dataSocketFd = socket(AF_INET, SOCK_STREAM, 0);
        prepare_socket(client->dataSocketFd);
        local.sin_addr.s_addr = htons(INADDR_ANY);;
        local.sin_family = AF_INET;
        local.sin_port = htons(client->dataPort);

        if (bind(client->dataSocketFd, (struct sockaddr *) &local, sizeof(local)) < 0) {
            respondClient(client, "425-Unable to bind data socket.\r\n425 %s.",
                    strerror(errno));
            return 1;
        }

        seteuid(curuid);

        client->dataSocketAddr.sin_family = AF_INET;
        if (connect(client->dataSocketFd, (struct sockaddr *) &client->dataSocketAddr, sizeof(client->dataSocketAddr)) == -1)
        {
            respondClient(client, "425-Unable to establish data connection.\r\n"
                    "425 %s.", strerror(errno));
            return 1;
        }
    }

    respondClient(client, "150 %s data connection established.",
                   client->xferType == EFXT_BINARY ? "BINARY" : "ASCII");
    return 0;
}

void do_stor(struct _EasyFtpClient *client, const char *filename, int flags)
{
    char *buffer;
    int fd, i, max;
    fd_set rfds;
    struct timeval tv;
    char *p, *pp;
    char *mapped = mapClientPathToServer(client, filename);

    int my_buffer_size = client->xferBufferSize;    /* total transfer buffer size divided by number of clients */
    int write_result;

    fd = open(mapped, flags, 00666);
    /*
         do this below
         if (mapped)
    free(mapped);
      */
    if (fd == -1)
    {
        efwarning("do_stor: '%s' while trying to store file '%s'.\n",
              strerror(errno), filename);
        respondClient(client, "553 Error: %s.", strerror(errno));

        close(fd);     /* make sure it is not open */
        free(mapped);
        return;
    }

    efinfo("do_stor: Client is storing file '%s'.\n", filename);
    if(create_data_transfer_socket(client))
    {
        close(fd);
        free(mapped);
        return;
    }

    buffer = malloc(my_buffer_size);
    /* Check to see if we are out of memory. -- Jesse */
    if (! buffer)
    {
       efwarning("do_stor: Unable to create buffer to receive file.\n");
       respondClient(client, "553 Error: An unknown error occured on the server.");

       close(fd);
       close(client->dataSocketFd);
       free(mapped);
       return;
    }

    lseek(fd, client->xferOffset, SEEK_SET);
    max = client->dataSocketFd + 1;
    for (;;)       /* start receiving loop */
    {
        FD_ZERO(&rfds);
        FD_SET(client->dataSocketFd, &rfds);

        tv.tv_sec = client->xferTimeout;
        tv.tv_usec = 0;
        if (! select(max, &rfds, NULL, NULL, &tv))
        {
            close(client->dataSocketFd);
            close(fd);
            respondClient(client, "426 Kicked due to data transmission timeout.");
            efwarning("do_stor: Kicked due to data transmission timeout.\n");
            exit(0);
        }

        /* Do not use the whole buffer, because a null byte has to be
         * written after the string in ASCII mode. */
        if (!((i = recv(client->dataSocketFd, buffer, my_buffer_size - 1, 0))))
        {
            break;
        }

        client->xferRecvedSize += i;

        if (client->xferType == EFXT_ASCII)
        {
            buffer[i] = '\0';
            /* on ASCII stransfer, strip character 13 */
            p = pp = buffer;
            while (*p)
            {
                if ((unsigned char) *p == 13)
                {
                    p++;
                }
                else
                {
                    *pp++ = *p++;
                }
            }
            *pp++ = 0;
            i = strlen(buffer);
        }     // end of if ASCII type transfer

        write_result = write(fd, buffer, i);
        if (write_result == -1)
        {
          break;
        }
    }     // end of for loop, reading

    free(mapped);
    free(buffer);
    close(fd);
    close(client->dataSocketFd);
    client->xferOffset = 0;

    respondClient(client, "226 File transmission successful.");
    efinfo("do_stor: File transmission successful.\n");

        // Update_Send_Recv(user, bytes_sent, bytes_recvd);
}

void easy_ftp_stat(char *name, FILE * client)
{
    struct stat statbuf;
    char temp[MAX_COMMAND_LENGTH + 6], linktarget[MAX_COMMAND_LENGTH + 5], perm[12], timestr[17],
        uid[MAX_USER_NAME_LENGTH + 1], gid[MAX_USER_NAME_LENGTH + 1];
    struct tm filetime;
    struct tm *tea_time, *local_time;
    time_t t;

    if (lstat(name, (struct stat *) &statbuf) == -1)
    {
        // used for command_stat
        fprintf(client, "213-Error: %s.\n", strerror(errno));
        return;
    }
#ifdef S_ISLNK
    if (S_ISLNK(statbuf.st_mode)) {
        strcpy(perm, "lrwxrwxrwx");
                memset(temp, '\0', sizeof(temp));
                readlink(name, temp, sizeof(temp) - 2);
                temp[MAX_COMMAND_LENGTH] = '\0';
        // temp[readlink(name, temp, sizeof(temp) - 1)] = '\0';
        sprintf(linktarget, " -> %s", temp);
    } else {
#endif
        strcpy(perm, "----------");
        if (S_ISDIR(statbuf.st_mode))
            perm[0] = 'd';
        if (statbuf.st_mode & S_IRUSR)
            perm[1] = 'r';
        if (statbuf.st_mode & S_IWUSR)
            perm[2] = 'w';
        if (statbuf.st_mode & S_IXUSR)
            perm[3] = 'x';
        if (statbuf.st_mode & S_IRGRP)
            perm[4] = 'r';
        if (statbuf.st_mode & S_IWGRP)
            perm[5] = 'w';
        if (statbuf.st_mode & S_IXGRP)
            perm[6] = 'x';
        if (statbuf.st_mode & S_IROTH)
            perm[7] = 'r';
        if (statbuf.st_mode & S_IWOTH)
            perm[8] = 'w';
        if (statbuf.st_mode & S_IXOTH)
            perm[9] = 'x';
        linktarget[0] = '\0';
#ifdef S_ISLNK
    }
#endif
    /* memcpy(&filetime, localtime(&(statbuf.st_mtime)), sizeof(struct tm)); */
    local_time = localtime(&(statbuf.st_mtime));
    if (! local_time)
    {
        return;
    }

    memcpy(&filetime, local_time, sizeof(struct tm));
    time(&t);
    tea_time = localtime(&t);
    if (! tea_time) return;
    /* if (filetime.tm_year == localtime(&t)->tm_year) */
    if (filetime.tm_year == tea_time->tm_year)
    {
        strncpy(timestr, ctime(&(statbuf.st_mtime)) + 4, 12);
        timestr[12] = 0;
    }
    else
    {
        strftime(timestr, sizeof(timestr), "%b %d  %G", &filetime);
    }

    memset(uid, 0, 8);
    strcpy(uid, DEFAULT_USER_ID_STRING);
    memset(gid, 0, 8);
    strcpy(gid, DEFAULT_USER_ID_STRING);

    fprintf(client, "%s %3i %-8s %-8s %llu %s %s%s\r\n", perm,
            (int) statbuf.st_nlink, uid, gid,
            (unsigned long long) statbuf.st_size,
            timestr, name, linktarget);
}

void dir_list_one_file(char *name, FILE *client, char verbose)
{
//    struct stat statbuf;
    char *filename_index;      /* place where filename starts in path */

/*
    if (! stat(name, (struct stat *) &statbuf))
    {
        efwarning("dir_list_one_file: could not get stat of file(%s): %s.\n", name, strerror(errno));
        return;
    }
*/
    /* find start of filename after path */
    filename_index = strrchr(name, '/');
    if (filename_index)
    {
       filename_index++;   /* goto first character after '/' */
    }
    else
    {
       filename_index = name;
    }

    if (verbose)
    {
        easy_ftp_stat(name, client);
    }
    else
    {
        fprintf(client, "%s\r\n", filename_index);
    }
}

void dir_list(struct _EasyFtpClient *ftpClient, char *name, FILE * client, char verbose, int show_hidden)
{
    DIR *directory = NULL;
    FILE *can_see_file;
    int show_nonreadable_files = FALSE;
    int show_hidden_files = FALSE;
    int skip_file, file_is_hidden;
    char *local_cwd = NULL;
    char *pattern = NULL, *short_pattern;
    struct dirent *dir_entry;

    if(show_hidden)
    {
        show_hidden_files = ftpClient->showHiddenFile;
    }

    show_nonreadable_files = ftpClient->showNonReadableFile;

    if ((strstr(name, "/.")) && strchr(name, '*'))
    {
        return; /* DoS protection */
    }

    if ((directory = opendir(name)))
    {
        closedir(directory);
        local_cwd = ftpClient->curWorkDir;
        chdir(name);
    }
    else
    {
        pattern = name;
    }

    directory = opendir(".");
    if (directory)
    {
        dir_entry = readdir(directory);
        while (dir_entry)
        {
            /* This check makes sure we skipped named pipes,
             which cannot be opened like normal files
             without hanging the server. -- Jesse
            */
#ifndef __minix
            if (dir_entry->d_type != DT_FIFO)
            {
#endif
                can_see_file = fopen(dir_entry->d_name, "r");
                if (can_see_file)
                {
                    fclose(can_see_file);
                }
                file_is_hidden = (dir_entry->d_name[0] == '.') ? TRUE : FALSE;
                skip_file = TRUE;

                if((! file_is_hidden) || (show_hidden_files) )
                {
                    if ( (can_see_file) || (show_nonreadable_files) )
                    {
                        if (pattern)
                        {
                            /* strip leading path */
                            short_pattern = strrchr(pattern, '/');
                            if (short_pattern)
                            {
                                short_pattern++;
                            }
                            else
                            {
                                short_pattern = pattern;
                            }
                            skip_file = fnmatch(short_pattern, dir_entry->d_name,
                            FNM_NOESCAPE);
                        }
                        else
                        {
                            skip_file = FALSE;
                        }
                    }
                }
                if (! skip_file)
                {
                    dir_list_one_file(dir_entry->d_name, client, verbose);
                }
#ifndef __minix
            }
#endif
            dir_entry = readdir(directory);
        }
        closedir(directory);
    }       /* unable to open directory */

    if (local_cwd) {
        chdir(local_cwd);
        free(local_cwd);
    }
}

void do_dir_list(struct _EasyFtpClient *client, const char *dirname, char verbose)
{
    int show_hidden = FALSE;
    FILE *datastream;

    if (dirname[0] != '\0')
    {
        /* check for show hidden files flag */
        if ( (dirname[0] == '-' ) && (dirname[1] == 'a') )
        {
            show_hidden = TRUE;
            while ((dirname[0] != ' ') && (dirname[0] != '\0'))
            {
                dirname++;
            }
            if (dirname[0] != '\0')
            {
                dirname++;
            }
        }
        /* skip other arguments */
        else if (dirname[0] == '-')
        {
            while ((dirname[0] != ' ') && (dirname[0] != '\0'))
            {
                dirname++;
            }
            if (dirname[0] != '\0')
            {
                dirname++;
            }
        }
    }

#if DEBUG_COMMANDS_IN_LINUX_PLATFORM == 0
    if (create_data_transfer_socket(client))
    {
        return;
    }
#endif

    datastream = fdopen(client->dataSocketFd, "w");
    if (dirname[0] == '\0')
    {
        dir_list(client, "*", datastream, verbose, show_hidden);
    }
    else
    {
        char *mapped = mapClientPathToServer(client, dirname);
        dir_list(client, mapped, datastream, verbose, show_hidden);
        free(mapped);
    }

    fclose(datastream);
    respondClient(client, "226 Directory list has been submitted.");
}

int easy_ftp_change_current_working_dir(struct _EasyFtpClient *client, const char *dirname)
{
    char *tmp = mapClientPathToServer(client, dirname);
    if(! tmp)
    {
        return -1;
    }

    if(chdir(tmp))
    {
        free(tmp);
        return errno;
    }

    strcpy(client->curWorkDir, tmp + strlen(client->ftpWorkDir));
    if(client->curWorkDir[0] == '\0')
    {
        strcpy(client->curWorkDir, "/");
    }
    umask(client->umaskForNew);
    free(tmp);
    return 0;
}
