#ifndef CONFIG_H
#define CONFIG_H

#define DEBUG_IN_LINUX_PLATFORM     (0)
#define DEBUG_COMMANDS_IN_LINUX_PLATFORM    (0)
#define DEBUG_USING_NETWORK_LOGGER  (0)

#define MAX_COMMAND_LENGTH (512)
#define MAX_REPSOND_BUFFER_LENGTH   (1024)
#define MAX_DIR_LENGTH      (1024)
#define MAX_USER_NAME_LENGTH    (30)

#define FALSE   (0)
#define TRUE    (1)

#define DEFAULT_LISTEN_PORT     (21)
#if DEBUG_IN_LINUX_PLATFORM
#define DEFAULT_WORKING_DIR     "/home/johnson/workspace"
#else
#define DEFAULT_WORKING_DIR     "/kotidata/almighty/recovery"
#endif
#define DEFAULT_PASV_PORT_START     (5600)
#define DEFAULT_PASV_PORT_END       (5700)
#define DEFAULT_DATA_PORT           (20)
#define DEFAULT_XFER_BUFFER_SIZE    (2048)
#define DEFAULT_XFER_TIME_OUT       (30)
#define DEFAULT_SHOW_HIDDEN_FILE    (1)
#define DEFAULT_SHOW_NON_READABLE_FILE  (0)
#define DEFAULT_UMASK_FOR_NEW_FILE  (022)

#define DEFAULT_USER_ID_STRING  "johnson"
#define DEFAULT_QUIT_MESSAGE    "Thanks to use Easy Ftp. Bye!"

#include "easyftplogger.h"
#define DEFAULT_LOGGER_LISTEN_PORT      (4401)

#endif // CONFIG_H
