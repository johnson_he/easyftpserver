#-------------------------------------------------
#
# Project created by QtCreator 2016-07-09T10:43:30
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = EasyFtp
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    easyftp/easyftpserver.c \
    easyftp/easyftpclient.c \
    easyftp/setting_parser.c \
    main.c \
    easyftp/easyftpcommand.c \
    easyftp/easyftplogger.c \
    easyftp/utils.c \
    easyftp/test.c

HEADERS += \
    easyftp/easyftpserver.h \
    easyftp/setting_parser.h \
    easyftp/easyftpclient.h \
    easyftp/easyftpcommand.h \
    easyftp/easyftplogger.h \
    easyftp/config.h \
    easyftp/utils.h \
    easyftp/test.h
