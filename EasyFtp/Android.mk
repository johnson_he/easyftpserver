LOCAL_PATH :=$(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE :=easyftpserver
LOCAL_SHARED_LIBRARIES := libc libcutils

LOCAL_CFLAGS += -Wno-error=format-security

LOCAL_C_INCLUDES +=$(LOCAL_PATH)/easyftp/

LOCAL_SRC_FILES :=$(call all-subdir-c-files)

LOCAL_MODULE_TAGS := eng
include $(BUILD_EXECUTABLE)

