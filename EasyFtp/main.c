#include "easyftp/easyftpserver.h"
#include "easyftp/config.h"

#if DEBUG_COMMANDS_IN_LINUX_PLATFORM
#include "easyftp/test.h"
#endif

int main(int argc, char *argv[])
{
    int ret = 0;
    struct _EasyFtpServer ftpServer;

    if(ef_init_logger() < 0)
    {
        printf("main: failed to init logger.\n");
    }

#if DEBUG_COMMANDS_IN_LINUX_PLATFORM
    return testCommands();
#else

    if(argc < 2)
    {
        printf("main: usage: easyftp RootDirPath.\n");
        return 0;
    }

    ret = initEasyFtpServer(&ftpServer, argv[1]);
    if(ret < 0)
    {
        printf("main: failed to parse setting.\n");
        return -1;
    }

    ret = startEasyFtpServer(&ftpServer);
    if(ret < 0)
    {
        printf("main: failed to start easy ftp server.\n");
        return -2;
    }

    return releaseEasyFtpServer(&ftpServer);
#endif
}
